//
//  ViewScreen1.swift
//  AppCats
//
//  Created by User on 16.10.2021.
//

import Foundation
import UIKit

class ViewScreen1: UIViewController {

    @IBOutlet var buttomTEst: UIButton!
    @IBOutlet var textFieldd: UITextField!
    let countries = ["USA", "Russia", "Brazil", "Germany", "Italy"]

    var pickerView = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.delegate = self
        pickerView.dataSource = self

        textFieldd.inputView = pickerView
        textFieldd.textAlignment = .center
    }}
    extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textFieldd.text = countries[row]
        textFieldd.resignFirstResponder()

    }
}
extension UIButton {
    func setRaunded() {
        self.layer.cornerRadius = 10
    }
}
