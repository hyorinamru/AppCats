//
//  UIButtonExtension.swift
//  AppCats
//
//  Created by User on 21.10.2021.
//

import UIKit
extension UIButton {
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        //duration - продолжительность анимации
        pulse.duration = 0.7
        pulse.fromValue = 0.60
        pulse.toValue = 1
        pulse.autoreverses = true
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.8
        pulse.damping = 1.2
        layer.add(pulse, forKey: nil)
    }
    func pulsateFalse() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.2
        pulse.fromValue = 0.8
        pulse.toValue = 1
        pulse.autoreverses = true
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.8
        pulse.damping = 1.2
        layer.add(pulse, forKey: nil)
    }
//    private func settwoGradient(colorOne: UIColor, colorTwo: UIColor) {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = bounds
//        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
//        gradientLayer.locations = [0.0, 1.0]
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
//        gradientLayer.endPoint  = CGPoint(x: 1.0, y: 1.0)
//        layer.insertSublayer(gradientLayer, at: 0)
//    }
}
