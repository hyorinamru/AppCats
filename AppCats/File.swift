//
//  File.swift
//  AppCats
//
//  Created by User on 13.11.2021.
//

import Foundation
import UIKit
class File: UIViewController {

    var imageToPresent: UIImage!
    var scrollView = UIScrollView()
    var imageView = UIImageView()
    var button = UIButton()
    init(displayedImage image: UIImage) {
        self.imageToPresent = image
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }
    override func viewDidLoad() {
        view.backgroundColor = .black
        setupScrollView()
        setupImagesView()
        buttonBack()
    }
    private func buttonBack() {
        button.setTitle("< Back", for: .normal)
        button.tintColor = .white
        button.frame = CGRect(x: view.safeAreaInsets.left + 10, y: view.safeAreaInsets.top + 70, width: 60, height: 60)
        button.addTarget(self, action: #selector(backTap), for: .touchUpInside)
        view.addSubview(button)
    }
    @objc private func backTap() {
            self.dismiss(animated: true, completion: nil)
        }

    private func setupScrollView() {
        scrollView.backgroundColor = .black
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        scrollView.maximumZoomScale = 4
        scrollView.minimumZoomScale = 1
        scrollView.delegate = self
    }
    private func setupImagesView() {
        imageView.image = self.imageToPresent
        imageView.backgroundColor = .black
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        scrollView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: scrollView.heightAnchor).isActive = true
    }
   
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
            if scrollView.zoomScale > 1 {
                if let image = imageView.image {
                    let ratioW = imageView.frame.width / image.size.width
                    let ratioH = imageView.frame.height / image.size.height

                    let ratio = ratioW < ratioH ? ratioW : ratioH
                    let newWidth = image.size.width * ratio
                    let newHeight = image.size.height * ratio
                    let conditionLeft = newWidth * scrollView.zoomScale > imageView.frame.width
                    let left = 0.5 * (conditionLeft ? newWidth - imageView.frame.width : (scrollView.frame.width - scrollView.contentSize.width))
                    let conditioTop = newHeight * scrollView.zoomScale > imageView.frame.height

                    let top = 0.5 * (conditioTop ? newHeight - imageView.frame.height : (scrollView.frame.height - scrollView.contentSize.height))

                    scrollView.contentInset = UIEdgeInsets(top: top, left: left, bottom: top, right: left)
                }
            } else {
                scrollView.contentInset = .zero
            }
        }
}

extension File: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
