//
//  Checkbox.swift
//  AppCats
//
//  Created by User on 06.11.2021.
//

import UIKit
import SwiftUI

class CategoriesListViewController: UIViewController {
    @IBOutlet var myTable: UITableView!
    var selArray1: [Int: String] = [:]
    weak var delegate: CustomDelegate?

    let phoneList: [String: String] = ["Abyssinian": "abys", "Aegean": "aege", "American Bobtail": "abob", "American Curl": "acur", "American Shorthair": "asho",
                     "American WireHair": "awir", "Arabian Mau": "amau", "Australian Mist": "amis", "Balinese": "bali", "Bambino": "bamb",
                     "bengal": "beng", "Birman": "birm", "Bombay": "bomb", "British longhair": "bslo", "British Shorthair": "bsho", "Burmese": "bure",
                     "Burmilla": "buri", "California Spangled": "cspa", "Chantilly-Tiffany": "ctif", "Chartreus": "char", "Chausie": "chau", "Cheetoh": "chee",
                     "Colorpoint Shothair": "csho", "Cornish Rex": "crex", "Cymric": "cymr", "Cyprus": "cypr", "Devon Rex": "drex", "Donskoy": "dons", "Dragon Li": "lihu",
                     "Egyptian Mau": "emau", "European Burmese": "ebur", "Exotic Shorthair": "esho", "havana Brown": "hbro", "Himalian": "hima", "Nihon Bobtail": "jbob",
                     "Javanese": "java", "Khao Manee": "khao", "Korat": "kora", "Kurilian": "kuri", "LaPerm": "lape", "Maine Coon": "mcoo", "malayan": "mala", "Manx": "manx",
                     "Munchkin": "munc", "Nebelung": "nebe", "Norbert": "norw", "Ocicat": "ocic", "Oriental": "orie", "persian": "pers", "Pixie-bob": "pixi", "Ragamuffi": "raga",
                     "Ragdoll": "ragd", "Russian Blue For MOTHER RUSSIAN": "rblu", "Savannah": "sava", "Scottish Fold": "sfol", "Selkirk Rex": "srex", "Siamese": "siam",
                     "Siberian": "sibe", "Singapura": "sing", "Snowshoe": "snow", "Somali": "soma", "Sphynx": "sphy", "Tonkinese": "tonk", "Toyger": "toyg",
                     "Turkish Angora": "tang", "Turkish Van": "tvan", "York Chocolate": "ycho"]

    override func viewDidLoad() {
        super.viewDidLoad()
        myTable.tableFooterView = UIView()
//        myTable.allowsMultipleSelectionDuringEditing = true
//        myTable.setEditing(true, animated: false)
        createFooter()
    }
    private func createFooter() {
        let customView = UIView()
        customView.backgroundColor = UIColor.white
        self.view.addSubview(customView)
        customView.translatesAutoresizingMaskIntoConstraints = false
        customView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        customView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        customView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        customView.topAnchor.constraint(equalTo: myTable.bottomAnchor).isActive = true
        customView.layer.cornerRadius = 18
        let button = UIButton(frame: customView.frame)
        button.backgroundColor = .white
        button.setTitle("Submit", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        customView.addSubview(button)
        button.layer.cornerRadius = 18
        button.translatesAutoresizingMaskIntoConstraints = false
        button.leadingAnchor.constraint(equalTo: customView.leadingAnchor).isActive = true
        button.trailingAnchor.constraint(equalTo: customView.trailingAnchor).isActive = true
        button.topAnchor.constraint(equalTo: customView.topAnchor).isActive = true
        button.heightAnchor.constraint(equalTo: customView.heightAnchor).isActive = true
    //        button.applyGradient(colours: [.yellow, .blue])
    }
    //function for button
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
        delegate?.test1(new: selArray1)
        navigationController?.popViewController(animated: true)
    }
    }

    extension CategoriesListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phoneList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let name = Array(phoneList.keys)[indexPath.row]
        cell.textLabel?.text = name
    //        cell.applyGradient(colours: [.yellow, .blue])
    //        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myTable.cellForRow(at: indexPath)?.accessoryType = .checkmark
//        print("select\(phoneList[indexPath.row])")
        let cell = tableView.cellForRow(at: indexPath)
        guard let text = cell?.textLabel?.text else { return }
        self.selArray1.updateValue(text, forKey: indexPath.row)
        dump(selArray1)
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        myTable.cellForRow(at: indexPath)?.accessoryType = .none
//        print("deselected\(phoneList[indexPath.row])")
        self.selArray1.removeValue(forKey: indexPath.row)
        dump(selArray1)
    }
    }
