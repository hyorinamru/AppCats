//
//  DiscImagesViewController.swift
//  AppCats
//
//  Created by User on 26.10.2021.
//

import UIKit
class DiscImagesViewController: UIViewController {
    //delegare for zoom picture
    var imageView: UIImageView!
    var pulseButton: CustomButton!
    var diskLabel: UILabel!
//    var imageSource: UIImage!

    private let imageSource: UIImage

    init(img: UIImage) {
        self.imageSource = img
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupImage()
        self.setupButton()
        self.setupLabel()
    }
        private func setupImage() {
        imageView = UIImageView()
        imageView.image = self.imageSource
        self.view.backgroundColor = .black
        self.view.addSubview(imageView)
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        imageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(test(_:)))
            imageView.addGestureRecognizer(tap)
            //pulseButton.frame.size = CGSize(width: 100, height: 100)
    }

    @objc func test (_ sender: UITapGestureRecognizer?) {
        let detailPhoto = File(displayedImage: self.imageSource)
        detailPhoto.modalPresentationStyle = .fullScreen
        detailPhoto.modalTransitionStyle = .flipHorizontal
        present(detailPhoto, animated: true, completion: nil)
    }
    private func setupButton() {
//        pulseButton = UIButton()
        pulseButton = CustomButton()
        pulseButton.setBackgroundImage(UIImage(systemName: "heart.fill"), for: .normal)
//        pulseButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        pulseButton.addTarget(self, action: #selector(self.pulseButtonPressed(_:)), for: .touchUpInside)
        self.view.addSubview(pulseButton)
//        pulseButton.imageView?.contentMode = .scaleAspectFit
        pulseButton.tintColor = .white
        pulseButton.translatesAutoresizingMaskIntoConstraints = false
        pulseButton.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20).isActive = true
        pulseButton.leftAnchor.constraint(equalTo: imageView.leftAnchor, constant: 8).isActive = true
        pulseButton.widthAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.05).isActive = true
        pulseButton.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.05).isActive = true
    }
    private func setupLabel() {
        diskLabel = UILabel()
        diskLabel.lineBreakMode = .byWordWrapping
        diskLabel.numberOfLines = 0
        diskLabel.text = "Breed:cat1 \n Categories:cat1"
        diskLabel.tintColor = .white
        diskLabel.backgroundColor = .darkGray
        self.view.addSubview(diskLabel)
        diskLabel.translatesAutoresizingMaskIntoConstraints = false
        diskLabel.topAnchor.constraint(equalTo: pulseButton.bottomAnchor, constant: 20).isActive = true
        diskLabel.leftAnchor.constraint(equalTo: pulseButton.leftAnchor, constant: 2).isActive = true
        diskLabel.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        diskLabel.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.05).isActive = true
    }

    @objc private func pulseButtonPressed(_ sender: CustomButton) {
//        let origImage = UIImage(systemName: "heart.fill")
//        let tintedImage = origImage? .withRenderingMode(.alwaysTemplate)
//        sender.setImage(tintedImage, for: .normal)
        sender.layer.shadowColor = UIColor .black.cgColor
        sender.layer.shadowOffset = CGSize(width: -1, height: 2)
        sender.layer.shadowOpacity = 0.8
        sender.layer.shadowRadius = 1.6
        sender.falseButton.toggle()
        if sender.falseButton {
            sender.tintColor = .systemRed
            sender.pulsate()
        } else {
            sender.tintColor = .white
            sender.pulsateFalse()
        }

    }
class CustomButton: UIButton {

    public var falseButton: Bool

    init() {
        self.falseButton = false
        super.init(frame: .zero)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }
}

//extension UIView {
//    func rotate() {
//        let rotation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
//        rotation.toValue = NSNumber(value: Double.pi * 2)
//        rotation.duration = 0.5
//        rotation.isCumulative = true
//        rotation.repeatCount = Float.greatestFiniteMagnitude
//        self.layer.add(rotation, forKey: "rotationAnimation")
//    }
//}
}
