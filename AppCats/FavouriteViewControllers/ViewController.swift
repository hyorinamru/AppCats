//
//  ViewController.swift
//  AppCats
//
//  Created by User on 09.10.2021.
//

import UIKit
import SwiftUI
//import SwiftUI
class ViewController: UIViewController, CustomDelegate {
    var data = [UIImage(named: "img1.png"), UIImage(named: "img2.jpeg"), UIImage(named: "img3.jpeg"), UIImage(named: "img4.jpeg"),
                UIImage(named: "img6.jpeg"), UIImage(named: "img2.jpeg"), UIImage(named: "img3.jpeg"), UIImage(named: "img4.jpeg")]
//    var breeds: [Int: String: String] = [:]
//    var categories: [Int: String: String] = [:]
//
    var dataImage: [Int: UIImage] = [:]
    var selArray: [Int: String] = [:]
    var selArray1: [Int: String] = [:]
    var tessPage: Int = 1
    // Создание элемента кнопка
    @IBOutlet var leftButton: UIButton!
    var collectionView: UICollectionView!
    var info: String = "Default"
    //базовая функция, в которой описывается состояние элементов
    override func viewDidLoad() {
        super.viewDidLoad()
        print("test")
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        // make sure that there is a slightly larger gap at the top of each row
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        // set a standard item size
        layout.itemSize = CGSize(width: self.view.frame.width, height: 450)
        // the layout scrolls vertical
        layout.scrollDirection = .vertical
        // set the frame and layout
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        // set the dataSource
        self.collectionView.dataSource = self
        // set the delegate
        self.collectionView.delegate = self
        // register the standard cell
        self.collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        // bounce at the bottom of the collection view
        self.collectionView.alwaysBounceVertical = true
        // set the background to be black
        self.collectionView.backgroundColor = .black
        self.view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: leftButton.bottomAnchor, constant: 20).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        getCats()
    }
    //Возвращение данных на экран ViewController из экрана Checkbox и CategoriesListViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? Checkbox {
            destination.delegate = self
        } else if let destination1 = segue.destination as? CategoriesListViewController {
            destination1.delegate = self
        }
    }
    // функция для обрабатывания массива selArray
    func test(new: [Int: String]) {
        print("test\n\(new)")
        self.selArray = new
    }
    func test1(new: [Int: String]) {
        print("test1\n\(new)")
        self.selArray1 = new
    }
    //функция результата парсинга ссылки с котами
    func getCats() {
        var url = URLComponents(string: "https://api.thecatapi.com/v1/images/search")!
        url.queryItems = [
            URLQueryItem(name: "limit", value: "10"),
            URLQueryItem(name: "page", value: String(self.tessPage)),
            URLQueryItem(name: "mime_types", value: "jpg, png")
           // URLQueryItem(name: "breed_id", value: "beng")
        ]
//        func getList() {
//            for (value) in ["breeds", "categories"] {
//                guard var url = URL(string: "https://api.thecatapi.com/v1/\(value)/") else { return }
//                print(url)
//                let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
//                    DispatchQueue.main.async {
//                        if error != nil {
//                            print(error.debugDescription)
//                        } else if data != nil {
//                            do {
//                                if value == "breeds" {
//                                    let list = try JSONDecoder().decode([Breed].self, from: data!)
//                                    for (value) in list {
//                                        print("breeds cpunt \(self.breedList.count)")
//                                        self.breedList.updateValue(value, forKey: value.id)
//                                    }
//                                    print("breeds count \(self.breedList.count)")
//                                } else {
//                                        let list = try JSONDecoder().decode([Category].self, from: data!)
//                                        for (value) in list {
//                                            print("\(value.id) \(value.name)")
//                                            self.categoriesList.updateValue(String(value.id), forKey: value.name)
//                                        }
//                                        print("categ count \(self.categoriesList.count)")
//                                    }
//                                } catch let error {
//                                    print(eroor.localizedDescription)
//                                    print(String(decoding: data!, as: UTF8.self))
//                                }
//                            }
//                        }
//                    }
//                    task.resume()
//                }
//            }
//        }
    //?????????????
        guard let absUrl = url.url?.absoluteURL else { return }
        print(absUrl)
        let task = URLSession.shared.dataTask(with: absUrl) { (data, _, error) in
            DispatchQueue.main.async {
                if error != nil {
                    print(error.debugDescription)
                } else if data != nil {
                    do {
                        let amount = self.dataImage.count
                        let myData = try JSONDecoder().decode([InfoCats].self, from: data!)
                        for (index, element) in myData.enumerated() {
                            let cur = UIImage(named: "test1.png")
                            self.dataImage.updateValue(cur!, forKey: index + amount)
                            self.getImage(stringUrl: element.url, index: index + amount)
                        }
                        //self.collectionView.reloadData()
                    } catch let error {
                        print(error.localizedDescription)
                    }
                }
            }
        }
        task.resume()
    }
    private func getImage(stringUrl url: String, index index: Int) {
        let imageUrl = URL(string: url)!

        URLSession.shared.dataTask(with: imageUrl, completionHandler: { (data, response, error) in
            if error != nil {
                print(error)
                return
            }
            DispatchQueue.main.async {
                if let data = data {
                    let downloadedImage = UIImage(data: data)
                    self.dataImage.updateValue(downloadedImage!, forKey: index)
                    self.collectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
                }
            }
        }).resume()
    }
}

extension ViewController: UICollectionViewDelegate {
    // if the user clicks on a cell, display a message
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let image = self.dataImage[indexPath.item] ?? UIImage(named: "cats.png")!
        let next = DiscImagesViewController(img: image)
        navigationController?.pushViewController(next, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if(indexPath.row == data.count - 1) {
            self.tessPage += 1
            getCats()
    }
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // the number of cells are wholly dependent on the number of colours
        dataImage.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // dequeue the standard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let imageView = UIImageView(frame: cell.bounds)
//        imageView.contentMode = .scaleAspectFill
        let item = self.dataImage[indexPath.item]
        imageView.frame = cell.bounds
        imageView.image = item
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        cell.addSubview(imageView)
        cell.backgroundColor = .black
//        cell.contentView.contentMode = .scaleAspectFill
        return cell
    }
}

protocol CustomDelegate: AnyObject {
    func test(new: [Int: String])
    func test1(new: [Int: String])
}
