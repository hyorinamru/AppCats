//
//  Checkbox.swift
//  AppCats
//
//  Created by User on 06.11.2021.
//

import UIKit
import SwiftUI

class Checkbox: UIViewController {
    @IBOutlet var myTable: UITableView!
    var selArray: [Int: String] = [:]
    weak var delegate: CustomDelegate?

    let phoneList: [String: String] = ["boxes": "5", "clothes": "15", "hats": "1", "sinks": "14", "space": "2", "sunglasses": "4", "ties": "7"]

    override func viewDidLoad() {
        super.viewDidLoad()
        myTable.allowsMultipleSelectionDuringEditing = true
        myTable.setEditing(true, animated: false)
        createFooter()
    }

    private func createFooter() {
        let customView = UIView()
        customView.backgroundColor = UIColor.white
        self.view.addSubview(customView)
        customView.translatesAutoresizingMaskIntoConstraints = false
        customView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        customView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        customView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        customView.topAnchor.constraint(equalTo: myTable.bottomAnchor).isActive = true
        customView.layer.cornerRadius = 18
        let button = UIButton(frame: customView.frame)
        button.backgroundColor = .white
        button.setTitle("Submit", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        customView.addSubview(button)
        button.layer.cornerRadius = 18
        button.translatesAutoresizingMaskIntoConstraints = false
        button.leadingAnchor.constraint(equalTo: customView.leadingAnchor).isActive = true
        button.trailingAnchor.constraint(equalTo: customView.trailingAnchor).isActive = true
        button.topAnchor.constraint(equalTo: customView.topAnchor).isActive = true
        button.heightAnchor.constraint(equalTo: customView.heightAnchor).isActive = true
//        button.applyGradient(colours: [.yellow, .blue])
    }
    //function for button
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
        delegate?.test(new: selArray)
        navigationController?.popViewController(animated: true)
    }
}

extension Checkbox: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phoneList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let name = Array(phoneList.keys)[indexPath.row]
        cell.textLabel?.text = name
//        cell.applyGradient(colours: [.yellow, .blue])
//        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("select\(phoneList[indexPath.row])")
        let cell = tableView.cellForRow(at: indexPath)
        guard let text = cell?.textLabel?.text else { return }
        self.selArray.updateValue(phoneList[text]!, forKey: indexPath.row)
        dump(selArray)
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        print("deselected\(phoneList[indexPath.row])")
        self.selArray.removeValue(forKey: indexPath.row)
        dump(selArray)
    }
}

//extension UIView {
//    @discardableResult
//    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
//         let gradient: CAGradientLayer = CAGradientLayer()
//         gradient.frame = self.bounds
//         gradient.colors = colours.map { $0.cgColor }
//         gradient.locations = [0.0,0.5]
//         self.layer.insertSublayer(gradient, at: 0)
//         return gradient
//    }
//}
