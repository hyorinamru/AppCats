//
//  ViewPickerController.swift
//  AppCats
//
//  Created by User on 22.10.2021.
//
import Photos
import PhotosUI
import UIKit
class ViewPickerController: UIViewController, PHPickerViewControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Picker Controller"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapAdd))
    }
    @objc private func didTapAdd() {
        var config = PHPickerConfiguration(photoLibrary: .shared())
        config.selectionLimit = 3
        config.filter = .images
        let vcc = PHPickerViewController(configuration: config)
        vcc.delegate = self
        present(vcc, animated: true)
    }
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true, completion: nil)
        results.forEach { result in
            result.itemProvider.loadObject(ofClass: UIImage.self) { reading, error in
                guard let image = reading as? UIImage, error == nil else {
                    return
                }
                print(image)
                }
            }
        }
    }
