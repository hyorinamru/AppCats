//
//  PhotoCollectionViewCell.swift
//  AppCats
//
//  Created by User on 26.10.2021.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    static let identifier = "PhotoCollectionViewCell"
    private let imageView: UIImageView = {

        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(imageView)
        let images = [
            UIImage(named: "cat1"),
            UIImage(named: "img2"),
            UIImage(named: "img3"),
            UIImage(named: "img4"),
            UIImage(named: "img6")
        ].compactMap({ $0 })
        imageView.image = images.randomElement()
    }
    required init?(coder: NSCoder) {
        fatalError("error")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = contentView.bounds
    }
    override func prepareForReuse() {
        super.prepareForReuse()

        //imageView.image = nil
    }
}
